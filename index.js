const path = require('path');
const http = require("http");
const stream = require("stream");
const { Image } = require('image-js');

// Получаем абсолютный путь до папки с изображениями
const imgDir = path.join(__dirname, 'images');

// Задаем тип контента
// Обратите внимание, что тип контента связан с типом (расширением) файла изображения
const contentType = 'image/png';

// Описываем метод-обработчик входящих запросов
const handler = function(request, response) {

    // Вызываем метод по получению изображения
    getImage(imgDir + '/dog.png', function(img, err) {
        if (!err) {// если все отработало без ошибок

            // Создаем объект - поток (stream)
            const readStream = new stream.PassThrough();

            // Устанавливаем тип возвращаемого контента
            response.writeHead(200, {'Content-Type': contentType });

            // Записываем полученное изображение в поток
            readStream.end(img);

            // Передаем поток в ответ (response)
            readStream.pipe(response);

        } else { // Если была ошибка
            console.log(err);
        }
    });
}

// Создаем и запускаем сервер
const server = http.createServer(handler);
server.listen(3000);


// Простая функция, которая берет файл-изображение и делает его ч/б,
// после чего выполянет переданную функцию callback
function getImage(fileName, callback) {
    // Метод асинхронный, поэтому используем then и catch
    // для обработки после выполнения метода
    Image.load(fileName)
        .then(function(img) {
            // Обрабатываем изображение            
            let grey = img.grey().toBuffer();
            // Вызываем callback-функцию и передаем в нее полученное
            // изображение в качестве буффера
            callback(grey);
        })
        .catch(function(err){
            // В случае ошибки вызываем callback-функцию
            // и передаем в нее ошибкук вторым аргументом
            callback(null, err);
        });
}