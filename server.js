const http = require("http");
const url = require('url');
  
const handler = function(request, response) {
    response.setHeader("Content-Type", "text/plain; charset=utf-8;");
    const requestUrl = new URL(request.url, `http://${request.headers.host}`)

    if(requestUrl.pathname === "/home" || requestUrl.pathname === "/") {
        const query = requestUrl.search.split('=');
        const result = Number(query[1])/2;
        response.write(`Результат деления на 2: ${result}`)
    } else if(requestUrl.pathname == "/about") {
        const year = requestUrl.searchParams.get('year');
        const pageName = requestUrl.searchParams.get('page');
        response.write(`Cайт опубликован в ${year} году и первая его страница называлась "${pageName}"`);
    } else {
        response.write("Not found");
    }
    
    response.end();
}

http.createServer(handler).listen(3000);

